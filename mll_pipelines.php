<?php
if (!defined("_ECRIRE_INC_VERSION")) return; 

function mll_insert_head_css($flux){
	// Insertion de la feuille de styles du menu de langues
	$css_mll = find_in_path('mll_styles.css');
	$flux .='<link rel="stylesheet" type="text/css" media="screen" href="'.$css_mll.'" />';

	return $flux;
}

// Ajouter les styles du login uniquement sur la bonne page
function mll_affichage_final($flux) {
	// Si on est sur la page de login ET que c'est une stylé avec les styles dist, pas un truc totalement perso
	if (strpos($flux, 'login_prive.css') !== false) {
		$css = '<link rel="stylesheet" type="text/css" media="screen" href="' . find_in_path('mll_login_dist.css') . '" />';
		$flux = str_replace('</head>', $css . '</head>', $flux);
	}
	
	return $flux;
}
