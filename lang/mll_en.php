<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/mll?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_mll' => 'Language menu as links',

	// E
	'explication_langues_invisibles' => 'Allows to remove some languages ​​from the menu',
	'explication_separateur' => 'Text used to divide the languages in the menu',

	// L
	'label_format' => 'Languages display format',
	'label_format_abrege' => 'Shortened (for instance, Fr)',
	'label_format_complet' => 'Complete (for instance, Français)',
	'label_format_liste' => 'Languages list display format',
	'label_format_liste_bloc' => 'A block divided by spaces',
	'label_format_liste_liste' => 'A bulleted list',
	'label_format_liste_liste_incluse' => 'A bulleted list contained in an existing list (no &lt;ul&gt; generated)',
	'label_langue_courante_invisible' => 'Hide the current language',
	'label_langue_site_enpremier' => 'Display the main language of the site first',
	'label_langues_invisibles' => 'Invisible languages in the​​ menu',
	'label_redirection' => 'When no translation is found',
	'label_redirection_accueil' => 'Redirect to the home page of the chosen language (if language sectors) or to the main home page. ',
	'label_redirection_self' => 'Stay on the current page ',
	'label_separateur' => 'Language divider'
);
