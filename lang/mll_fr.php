<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/menu_langues_liens.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_mll' => 'Menu de langues sous forme de liens',

	// E
	'explication_langues_invisibles' => 'Permet d’enlever certaines langues du menu',
	'explication_separateur' => 'Texte utilisé pour séparer les langues dans le menu',

	// L
	'label_format' => 'Format d’affichage des langues',
	'label_format_abrege' => 'Abrégé (par exemple, Fr)',
	'label_format_complet' => 'Complet (par exemple, Français)',
	'label_format_liste' => 'Format de la liste des langues',
	'label_format_liste_bloc' => 'Un bloc séparé par des espaces (ou séparateur configuré)',
	'label_format_liste_liste' => 'Une liste à puces',
	'label_format_liste_liste_incluse' => 'Une liste à puces contenue dans une liste existante (pas de &lt;ul&gt; généré)',
	'label_langue_courante_invisible' => 'Masquer la langue courante',
	'label_langue_site_enpremier' => 'Afficher la langue principale du site en premier',
	'label_langues_invisibles' => 'Langues invisibles du menu',
	'label_redirection' => 'Lorsqu’aucune traduction n’est trouvée',
	'label_redirection_accueil' => 'Rediriger vers l’accueil de la langue choisie (si secteurs de langue) ou l’accueil principal',
	'label_redirection_self' => 'Rester sur la page en cours',
	'label_separateur' => 'Séparateur des langues'
);
